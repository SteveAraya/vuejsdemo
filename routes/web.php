<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/rol', 'RolController@index');

Route::get('/user', 'UserController@index');
Route::post('/user/register', 'UserController@store');
Route::put('/user/update', 'UserController@update');



Route::group(['middleware'=>['guest']],function(){

    Route::get('/rol', 'RolController@index');

    Route::get('/user', 'UserController@index');
    Route::post('/user/register', 'UserController@store');
    Route::put('/user/update', 'UserController@update');

});