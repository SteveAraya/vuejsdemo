<?php

namespace App\Http\Controllers;
use App\User;
use App\People;
use Illuminate\Support\Facades\DB;
 
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
 
        $search = $request->search;
        $criterio = $request->criterio;
         
        if ($search==''){
            $user = User::join('roles','users.idrol','=','roles.id')
            ->select('users.id','users.firstName','users.lastName',
            'users.email','users.phone','users.address','users.password',
            'users.idrol','roles.name as rol')
            ->orderBy('users.id', 'desc')->paginate(5);
        }
        else{
            $user = User::join('roles','users.idrol','=','roles.id')
            ->select('users.id','users.firstName','users.lastName',
            'users.email','users.phone','users.address','users.password',
            'users.idrol','roles.name as rol')            
            ->where('users.'.$criterio, 'like', '%'. $search . '%')
            ->orderBy('users.id', 'desc')->paginate(3);
        }
         
 
        return [
            'pagination' => [
                'total'        => $user->total(),
                'current_page' => $user->currentPage(),
                'per_page'     => $user->perPage(),
                'last_page'    => $user->lastPage(),
                'from'         => $user->firstItem(),
                'to'           => $user->lastItem(),
            ],
            'user' => $user
        ];
    }
 
    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
         
        try{
            DB::beginTransaction();
 
            $user = new User();
            $user->firstName = $request->firstName;
            $user->lastName = $request->lastName;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->address = $request->address;
            $user->email = $request->email;
            $user->password = bcrypt( $request->password);
            $user->idrol = $request->idrol;          
 
            $user->save();
 
            DB::commit();
 
        } catch (Exception $e){
            DB::rollBack();
        }
  
    }
 
    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
         
        try{
            DB::beginTransaction();
 
            //search primero el proveedor a modificar
            $user = User::findOrFail($request->id);

            $user->firstName = $request->firstName;
            $user->lastName = $request->lastName;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->address = $request->address;
            $user->email = $request->email;
            $user->password = bcrypt( $request->password);
            $user->idrol = $request->idrol;          
 
            $user->save();
 
            DB::commit();
 
        } catch (Exception $e){
            DB::rollBack();
        }
    }
}
