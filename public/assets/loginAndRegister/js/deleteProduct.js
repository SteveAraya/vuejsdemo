function preguntarSiNo(id) {
    alertify.confirm('Delete Product', 'Are you sure to delete this product?',
        function() { eliminarDatos(id) },
        function() { alertify.error('it was canceled') });
}

function eliminarDatos(id) {

    cadena = "id=" + id;

    $.ajax({
        type: "POST",
        url: "deleteProduct.php",
        data: cadena,
        success: function(r) {
            console.log(r);
            if (r == 1) {
                location.href = '../product/product.php';
            } else {
                alertify.error("The server failed :(");
            }
        }
    });
}