jQuery(document).on('submit', '#registerForm', function(event) {
    event.preventDefault();

    jQuery.ajax({
            url: '../validate/validateRegister.php',
            type: 'POST',
            dataType: 'json',
            data: $(this).serialize(),
            beforeSend: function() {
                $('.btnrg').val('Validating...');
            }
        })
        .done(function(answer) {
            if (!answer.error) {
                location.href = '../homeclient.php';
            } else {
                $('.error').slideDown('slow');
                setTimeout(function() {
                    $('.error').slideUp('slow');
                }, 3000);
                $('.btnrg').val('Register');
            }
        })
        .fail(function(resp) {
            console.log("Faild");
            console.log(resp.responseText);
        })
        .always(function() {
            console.log("complete");
        });
});