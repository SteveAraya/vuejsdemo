jQuery(document).on('submit', '#createProduct', function(event) {
    event.preventDefault();

    var formData = new FormData($("#createProduct")[0]);

    jQuery.ajax({
            url: '../product/updateProduct.php',
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
        })
        .done(function(answer) {

            if (!answer.error) {

                swal("Good job!", "Click on OK!", "success")
                swal({
                        title: "Good job?",
                        text: "The product was stored successfully. Click on OK!",
                        type: "success",
                        confirmButtonText: "OK!",
                        closeOnConfirm: false
                    },
                    function() {
                        location.href = '../product/product.php';
                    });
            } else {

                if (answer.type === 'category') {
                    swal({
                        title: 'Error',
                        text: 'You have to choose a category, please',
                        html: '<p>Mensaje de texto con <strong>formato</strong>.</p>',
                        type: "warning",
                        timer: 5000,
                    });

                } else {
                    swal({
                        title: 'Error',
                        text: 'The SKU you chose, already used in another product, choose another SKU please',
                        html: '<p>Mensaje de texto con <strong>formato</strong>.</p>',
                        type: "warning",
                        timer: 5000,
                    });
                }
            }
        })
        .fail(function(resp) {
            console.log(resp.responseText);
        })
        .always(function() {
            console.log("complete");
        });
});