
function add(){
    read_form("add","treeview_form");
}

function delete_row()
{
    read_form("remove","treeviewActions");
}

function edit_row(){
    read_form("edit","treeviewActions");
}

function read_form(method_selected, form_selected){
    var idform = "#"+form_selected;
    var formParams = $(idform).serialize() + "&methodSelected=" + method_selected;
    console.log("Hello from crud.js " + formParams);

    $.ajax({
        url:"/categoryTreeView/category-controller.php",
        method:"POST",
        data: formParams,
        success:function(data){  
            set_message(data,idform);
            console.log(data);
        },
        error: function () {
            console.log('error from treeviewActions');
        }
    })
}

function set_message(data,idform){
    if(data !== "succesful"){
        alert(data);
        return;
    }
    else{
        fill_treeview();
        fill_parent_category();
        $(idform)[0].reset();
    }
}

function fill_treeview()
{
    var methodSelected = "methodSelected=fetch";
    $.ajax({
        url:"/categoryTreeView/category-controller.php",
        method:"POST",
        data:methodSelected,
        dataType:"json",
        success:function(data){
            $('#treeview').treeview({
                data:data
            });
        },
        error: function (event) {
            console.log('error from treeviewActions',event);
        }
    })  
}

function fill_parent_category()
{
    var methodSelected = "methodSelected=fill";
    $.ajax({
        url:"/categoryTreeView/category-controller.php",
        method:"POST",
        data:methodSelected,
        success:function(data){
            $('#parent_category').html(data);
            $('#category_select').html(data);
        }
    });
}