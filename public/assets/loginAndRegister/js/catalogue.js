function show_details(){
    var productid = event.srcElement.id;
    console.log(productid);
    //window.location.replace();
    window.location.href = "/product-details.php?productid="+productid;
}

function fill_treeview()
{
    var methodSelected = "methodSelected=fetch";
    $.ajax({
        url:"/cataloguecomponent/catalogue-controller.php",
        method:"POST",
        data:methodSelected,
        dataType:"json",
        selectedIcon:"fab fa-accessible-icon",
        success:function(data){
            $('#treeview').treeview({
                data:data
            });
        },
        error: function (event) {
            console.log('error from treeviewActions',event);
        }
    })  
}
