jQuery(document).on('submit', '#loginForm', function(event) {
    event.preventDefault();

    jQuery.ajax({
            url: '../validate/validateUser.php',
            type: 'POST',
            dataType: 'json',
            data: $(this).serialize(),
            beforeSend: function() {
                $('.btnlg').val('Validating...');
            }
        })
        .done(function(answer) {
            if (!answer.error) {
                if (answer.rol == 1) {
                    location.href = '../home.php';
                } else if (answer.rol == 2) {
                    location.href = '../homeclient.php';
                }
            } else {
                $('.error').slideDown('slow');
                setTimeout(function() {
                    $('.error').slideUp('slow');
                }, 3000);
                $('.btnlg').val('Login');
            }
        })
        .fail(function(resp) {
            console.log(resp.responseText);
        })
        .always(function() {
            console.log("complete");
        });
});