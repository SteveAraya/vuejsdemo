$(document).ready(function() {
    $.getJSON('/sale/sale_ajax.php', function(json) {

        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        var chart_data = [
            ['Date', 'Amount']
        ];

        json.forEach(function(item) {
            chart_data.push([item.date, parseFloat(item.sales)]);
        });

        function drawChart() {
            var data = google.visualization.arrayToDataTable(chart_data);
            var options = {
                title: 'Sales per Date'
            };
            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
        }

    });

});